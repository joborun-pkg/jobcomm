#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------
# Contributor	: Evhorizon <event.horizon@disroot.org>

pkgname=spacefm
pkgver=1.0.6
pkgrel=06
pkgdesc='Multi-panel tabbed file manager - GTK3 - w/o ffmpegthumbnailer'
url="https://ignorantguru.github.io/spacefm/"
makedepends=(intltool gettext libglvnd gtk3)
conflicts=(spacefm-git spacefm-thermitegod spacefm-gtk2)
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/IgnorantGuru/spacefm/archive/${pkgver}.tar.gz"
        "https://github.com/FabioLolix/AUR-artifacts/raw/master/spacefm-Fix-GCC-10-build.patch"
        "https://raw.githubusercontent.com/FabioLolix/AUR-artifacts/master/spacefm-glibc-2.28-compatibility.patch")

prepare() {
  cd "$srcdir/${pkgname}-${pkgver}"
  patch -Np1 -i ../spacefm-glibc-2.28-compatibility.patch
  patch -Np1 -i ../spacefm-Fix-GCC-10-build.patch
}

build() {
  CFLAGS+=" -Wno-error=incompatible-pointer-types"

  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-fast-install \
		--disable-desktop-integration \
		--disable-startup-notification \
		--disable-pixmaps \
		--disable-video-thumbnails \
		--with-gtk3 \
		--without-gtk2
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  depends=(gtk3 startup-notification ) # ffmpegthumbnailer) build runs into error due to svt-av1
  optdepends=('dbus: dbus integration'
            'util-linux: disk eject support'
            'lsof: device processes'
            'wget: plugin download'
            'gksu: perform as root functionality'
            'curlftpfs: mount FTP shares'
            'jmtpfs: mount MTP devices'
            'gphotofs: mount cameras'
            'ifuse: mount your iPhone/iPod Touch'
            'fuseiso: mount ISO files'
	  'spacefm-plugin-clamav')

	make DESTDIR="$pkgdir" install
	rm -f "$pkgdir"/usr/bin/spacefm-installer
	install -Dm644 "COPYING" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(GPL3)

sha256sums=('fedea9fcad776e0af4b8d90c5a1c86684a9c96ef1cdd4e959530ce93bdebe7c9'
            '16622d0d56c40e87e846a81709d9c2c8303f189e53a783bf20ccdb57b8f9465f'
            '12411055df994211d2968cb52746b6caefce6926aed1ed33b542bd70b571ce7e')

##  81a87460d2263d4b93f11b3e1c0af9363191dce9d38f725f0493a60835a3f2b0  spacefm-1.0.6-06-x86_64.pkg.tar.lz

