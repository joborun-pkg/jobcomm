`configure' configures spacefm 1.0.6 to adapt to many kinds of systems.

Usage: src/spacefm-1.0.6/configure [OPTION]... [VAR=VALUE]...

To assign environment variables (e.g., CC, CFLAGS...), specify them as
VAR=VALUE.  See below for descriptions of some of the useful variables.

Defaults for the options are specified in brackets.

Configuration:
  -h, --help              display this help and exit
      --help=short        display options specific to this package
      --help=recursive    display the short help of all the included packages
  -V, --version           display version information and exit
  -q, --quiet, --silent   do not print `checking ...' messages
      --cache-file=FILE   cache test results in FILE [disabled]
  -C, --config-cache      alias for `--cache-file=config.cache'
  -n, --no-create         do not create output files
      --srcdir=DIR        find the sources in DIR [configure dir or `..']

Installation directories:
  --prefix=PREFIX         install architecture-independent files in PREFIX
                          [/usr/local]
  --exec-prefix=EPREFIX   install architecture-dependent files in EPREFIX
                          [PREFIX]

By default, `make install' will install all the files in
`/usr/local/bin', `/usr/local/lib' etc.  You can specify
an installation prefix other than `/usr/local' using `--prefix',
for instance `--prefix=$HOME'.

For better control, use the options below.

Fine tuning of the installation directories:
  --bindir=DIR            user executables [EPREFIX/bin]
  --sbindir=DIR           system admin executables [EPREFIX/sbin]
  --libexecdir=DIR        program executables [EPREFIX/libexec]
  --sysconfdir=DIR        read-only single-machine data [PREFIX/etc]
  --sharedstatedir=DIR    modifiable architecture-independent data [PREFIX/com]
  --localstatedir=DIR     modifiable single-machine data [PREFIX/var]
  --libdir=DIR            object code libraries [EPREFIX/lib]
  --includedir=DIR        C header files [PREFIX/include]
  --oldincludedir=DIR     C header files for non-gcc [/usr/include]
  --datarootdir=DIR       read-only arch.-independent data root [PREFIX/share]
  --datadir=DIR           read-only architecture-independent data [DATAROOTDIR]
  --infodir=DIR           info documentation [DATAROOTDIR/info]
  --localedir=DIR         locale-dependent data [DATAROOTDIR/locale]
  --mandir=DIR            man documentation [DATAROOTDIR/man]
  --docdir=DIR            documentation root [DATAROOTDIR/doc/spacefm]
  --htmldir=DIR           html documentation [DOCDIR]
  --dvidir=DIR            dvi documentation [DOCDIR]
  --pdfdir=DIR            pdf documentation [DOCDIR]
  --psdir=DIR             ps documentation [DOCDIR]

Program names:
  --program-prefix=PREFIX            prepend PREFIX to installed program names
  --program-suffix=SUFFIX            append SUFFIX to installed program names
  --program-transform-name=PROGRAM   run sed PROGRAM on installed program names

System types:
  --build=BUILD     configure for building on BUILD [guessed]
  --host=HOST       cross-compile to build programs to run on HOST [BUILD]

Optional Features:
  --disable-option-checking  ignore unrecognized --enable/--with options
  --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
  --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
  --enable-silent-rules   less verbose build output (undo: "make V=1")
  --disable-silent-rules  verbose build output (undo: "make V=0")
  --enable-maintainer-mode
                          enable make rules and dependencies not useful (and
                          sometimes confusing) to the casual installer
  --enable-static[=PKGS]  build static libraries [default=no]
  --enable-dependency-tracking
                          do not reject slow dependency extractors
  --disable-dependency-tracking
                          speeds up one-time build
  --enable-shared[=PKGS]  build shared libraries [default=yes]
  --enable-fast-install[=PKGS]
                          optimize for fast installation [default=yes]
  --disable-libtool-lock  avoid locking (might break parallel builds)
  --disable-nls           do not use Native Language Support
  --disable-startup-notification
                          disable use of libstartup-notification (default:
                          enable if installed)
  --enable-cast-checks    enable Glib casting checks (default: disable)
  --enable-hal            build with Linux HAL support (disables udisks)
                          (default: no)
  --disable-inotify       disable Linux inotify kernel support (requires
                          fam/gamin instead) (default: enable)
  --enable-largefile      enable Large file support (default: yes)
  --disable-superuser-checks
                          disable checks running as super user (no current
                          function) (default: enable)
  --disable-desktop-integration
                          disable desktop manager integration (default:
                          enable)
  --disable-video-thumbnails
                          disable libffmpegthumbnailer video thumbnails
                          (default: enable)
  --enable-pixmaps        use share/pixmaps dir instead of share/icons dir to
                          store icons (default: disable)

Optional Packages:
  --with-PACKAGE[=ARG]    use PACKAGE [ARG=yes]
  --without-PACKAGE       do not use PACKAGE (same as --with-PACKAGE=no)
  --with-pic[=PKGS]       try to use only PIC/non-PIC objects [default=use
                          both]
  --with-gnu-ld           assume the C compiler uses GNU ld [default=no]
  --with-sysroot=DIR Search for dependent libraries within DIR
                        (or the compiler's sysroot if not specified).
  --with-bash-path=PATH   Absolute path to GENUINE bash v4 - other shells lack
                          features REQUIRED by SpaceFM (default: /bin/bash)
  --with-gtk3             Build with GTK3 interface instead of GTK2 (default:
                          with GTK2 unless GTK2 not installed)
  --with-gtk2             Build with GTK2 interface only (default: with GTK2
                          unless GTK2 not installed)
  --with-preferable-sudo=PROG
                          Specify custom graphical su program to run root
                          commands (or see /etc/spacefm/spacefm.conf)

Some influential environment variables:
  CC          C compiler command
  CFLAGS      C compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
  LIBS        libraries to pass to the linker, e.g. -l<library>
  CPPFLAGS    (Objective) C/C++ preprocessor flags, e.g. -I<include dir> if
              you have headers in a nonstandard directory <include dir>
  CPP         C preprocessor
  PKG_CONFIG  path to pkg-config utility
  PKG_CONFIG_PATH
              directories to add to pkg-config's search path
  PKG_CONFIG_LIBDIR
              path overriding pkg-config's built-in search path
  GTK_CFLAGS  C compiler flags for GTK, overriding pkg-config
  GTK_LIBS    linker flags for GTK, overriding pkg-config
  SN_CFLAGS   C compiler flags for SN, overriding pkg-config
  SN_LIBS     linker flags for SN, overriding pkg-config
  HAL_CFLAGS  C compiler flags for HAL, overriding pkg-config
  HAL_LIBS    linker flags for HAL, overriding pkg-config
  LIBUDEV_CFLAGS
              C compiler flags for LIBUDEV, overriding pkg-config
  LIBUDEV_LIBS
              linker flags for LIBUDEV, overriding pkg-config
  FFMPEG_CFLAGS
              C compiler flags for FFMPEG, overriding pkg-config
  FFMPEG_LIBS linker flags for FFMPEG, overriding pkg-config

Use these variables to override the choices made by `configure' or to help
it to find libraries and programs with nonstandard names/locations.

Report bugs to the package provider.
