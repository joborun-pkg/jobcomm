#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://gittea.disroot.org/joborun-pkg/jobcomm/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=pyside2
pkgname=(shiboken2 python-shiboken2 pyside2 pyside2-tools)
_qtver=5.15.13
_clangver=17.0.6
pkgver=${_qtver/-/}
pkgrel=03
url='https://www.qt.io'
makedepends=(cmake python-setuptools python-wheel llvm clang=$_clangver
             qt5-multimedia qt5-tools qt5-sensors qt5-charts qt5-webengine qt5-datavis3d
             qt5-websockets qt5-speech qt5-3d qt5-svg qt5-script qt5-scxml qt5-x11extras
             qt5-quickcontrols2 qt5-serialport qt5-remoteobjects qt5-xmlpatterns)
optdepends=('qt5-svg: QtSvg bindings'
            'qt5-script: QtScript bindings'
            'qt5-speech: QtTextToSpeech bindings'
            'qt5-websockets: QtWebSockets bindings'
            'qt5-webengine: QtWebEngine bindings'
            'qt5-datavis3d: QtDataVisualization bindings'
            'qt5-scxml: QtScxml bindings'
            'qt5-sensors: QtSensors bindings'
            'qt5-3d: Qt3D bindings'
            'qt5-x11extras: QtX11Extras bindings'
            'qt5-charts: QtCharts bindings'
            'qt5-tools: QtHelp bindings'
            'qt5-remoteobjects: QtRemoteObjects bindings'
            'qt5-serialport: QtSerialPort bindings'
            'qt5-quickcontrols2: QtQuickControls2 bindings')
_pkgfqn=pyside-setup-opensource-src-$_qtver
source=(https://download.qt.io/official_releases/QtForPython/pyside2/PySide2-$pkgver-src/${_pkgfqn}.tar.xz)

prepare(){
  cd "$srcdir/$_pkgfqn"

  # https://github.com/python/cpython/issues/118777
  sed '/PyObject \*dict = type->tp_dict;/a\    if (dict == NULL) dict = PyType_GetDict(type);' -i sources/shiboken2/libshiboken/signature/signature_helper.cpp

  # https://github.com/arch4edu/arch4edu/issues/268
  sed '/typing.TypeVar.__repr__ = _typevar__repr__/d' -i sources/pyside2/PySide2/support/generate_pyi.py

  sed '/check_allowed_python_version()/d' -i setup.py
}

build() {
  cmake -B build -S $_pkgfqn \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=None \
    -DBUILD_TESTS=OFF \
    -DPYTHON_EXECUTABLE=/usr/bin/python
  cmake --build build
}

package_shiboken2() {
  pkgdesc='Generates bindings for C++ libraries using CPython source code'
  depends=(clang=$_clangver llvm libxslt qt5-xmlpatterns)

  DESTDIR="$pkgdir" cmake --install build/sources/shiboken2
# Provided in python-shiboken2
  rm -r "$pkgdir"/usr/lib/{python*,libshiboken*}
# Conflicts with shiboken6 and doesn't work anyway
  rm "$pkgdir"/usr/bin/shiboken_tool.py
}

package_python-shiboken2() {
  pkgdesc='Python bindings for shiboken2'
  depends=(python)

  DESTDIR="$pkgdir" cmake --install build/sources/shiboken2
# Provided in shiboken2
  rm -r "$pkgdir"/usr/{bin,include,lib/{cmake,pkgconfig}}

# Install egg-info
  cd $_pkgfqn
  python setup.py egg_info --build-type=shiboken2
  _pythonpath=`python -c "from sysconfig import get_path; print(get_path('platlib'))"`
  cp -r shiboken2.egg-info "$pkgdir"/$_pythonpath
}

package_pyside2() {
  pkgdesc='Enables the use of Qt5 APIs in Python applications'
  depends=(python-shiboken2 qt5-declarative)
  optdepends=('qt5-svg: QtSvg bindings'
              'qt5-script: QtScript bindings'
              'qt5-speech: QtTextToSpeech bindings'
              'qt5-websockets: QtWebSockets bindings'
              'qt5-webengine: QtWebEngine bindings'
              'qt5-datavis3d: QtDataVisualization bindings'
              'qt5-scxml: QtScxml bindings'
              'qt5-sensors: QtSensors bindings'
              'qt5-3d: Qt3D bindings'
              'qt5-x11extras: QtX11Extras bindings'
              'qt5-charts: QtCharts bindings'
              'qt5-tools: QtHelp bindings'
              'qt5-remoteobjects: QtRemoteObjects bindings'
              'qt5-serialport: QtSerialPort bindings'
              'qt5-quickcontrols2: QtQuickControls2 bindings')
  provides=(qt5-python-bindings)

  DESTDIR="$pkgdir" cmake --install build/sources/pyside2
# Install egg-info
  cd $_pkgfqn
  python setup.py egg_info --build-type=pyside2
  _pythonpath=`python -c "from sysconfig import get_path; print(get_path('platlib'))"`
  cp -r PySide2.egg-info "$pkgdir"/$_pythonpath
}

package_pyside2-tools() {
  pkgdesc='Tools for PySide2'
  depends=(pyside2)

  DESTDIR="$pkgdir" cmake --install build/sources/pyside2-tools
  rm "$pkgdir"/usr/bin/{rcc,uic,designer,pyside_tool.py} # provided by qt5-base
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=(LGPL)

sha256sums=(7a57797b20268d6ebcb39deba48c754a69abf9221aee03e1f3dca6f6565b7da9) # pyside-setup-opensource-src-5.15.13.tar.xz

##  8b248d888119f4e512d5e644047bfd3fa5db959a9f468a041ad3512eab67e6d4  pyside2-5.15.13-03-x86_64.pkg.tar.lz
##  cdfc23fc492e8a656430004b09b71570436b5024ab3421b0d34b14f8126e1fca  pyside2-tools-5.15.13-03-x86_64.pkg.tar.lz
##  679a7652b67ee94c75a993a7f757598008ad4e8cb5a37abb8cb17d9c2fd0ef78  python-shiboken2-5.15.13-03-x86_64.pkg.tar.lz
##  16e2f7ac8a8eba71ed2d4d5e23126fff2da19b84a7b8fe2ed88b618b31249a98  shiboken2-5.15.13-03-x86_64.pkg.tar.lz

