# jobcomm

joborun's community repository for the community by the community

Many AUR pkgs and their dependencies built or waiting for you to build them utilizing Joborun's 
build model.

Joborun team

The repository should go between jobextra and obcore and look like this

/etc/pacman.conf:


    [jobcomm]
    #Server = file:///var/cache/jobcomm/       ## for local repository use
    Include = /etc/pacman.d/mirrorlist-jobo

/etc/pacman.d/mirrorlist-jobo

    Server = http://downloads.sourceforge.net/joborun/r


You can switch between local and sf after building what you are interested in.

Clone the repository into /src/pkg and [[build|https://git.disroot.org/joborun/web/src/branch/main/howto.md]] the package of choice:

    cd /src/pkg
    git clone https://git.disroot.org/joborun-pkg/jobcomm.git jobcomm
    cd jobcomm

