#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgbase=smdev-phkr
pkgname=smdev
pkgver=0.2.3
pkgrel=011
_rev=8d07540
pkgdesc='Suckless mdev - alternative to udevd use in conjunction with libudev-zero - experimental use only'
url="http://git.suckless.org/smdev/"
makedepends=( 'git' )
provides=('mdev')
options=( 'strip' )
install='smdev.install'
source=("git+https://git.suckless.org/smdev#commit=${_rev}"
        'scan_all.patch'
        'glibc.patch'
        'usb_nodes.patch'
        'config.h'
        'processdev'
        '00-modprobe'
        '99-remove_links'
        'initcpio.hook'
        'initcpio.install'
        'smdev.install')

prepare() {
	cd "${srcdir}/${pkgname}"
	cp "${srcdir}/config.h" config.h
	patch -Np1 -i ../scan_all.patch
	patch -Np1 -i ../glibc.patch
	patch -Np1 -i ../usb_nodes.patch
}

build() {
	cd "${srcdir}/${pkgname}"
	make clean
	cp ../config.h ./
	make
}

package() {
	cd "${srcdir}/${pkgname}"
	make PREFIX=/usr DESTDIR="$pkgdir" install
	cd "${srcdir}"
	install -m744 -D processdev ${pkgdir}/etc/smdev/processdev
	install -m644 -D 00-modprobe $pkgdir/etc/smdev/00-modprobe
	install -m644 -D 99-remove_links $pkgdir/etc/smdev/remove/99-remove_links
	install -m644 -D initcpio.hook $pkgdir/usr/lib/initcpio/hooks/smdev
	install -m644 -D initcpio.install $pkgdir/usr/lib/initcpio/install/smdev
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=( 'MIT/X' )

sha1sums=('SKIP'
                  'a90bd08c8c482dec4b8cc063b4841db034ea7e1e'
                  'b300e68de6bcb6f542bb28206ab10f6ceca242bc'
                  'f0e3bfc2956d0c8885e08732cff8d0f9b6cf7103'
                  '1f6b9327866981ffab4a275a64ee5c5249038968'
                  'd741887a118ed921528189754d886159782ca6fe'
                  '396e84460ba95dcf3aeaaf9f2bc711be92a2c395'
                  '2fa17e2c0ab472fb1ec7a1b04630393df67cf07a'
                  '7fe74315c9a3592c2eb66cea8cd652da04461786'
                  'ea04c776d0f6b749bfd1fc421d51c5ecd07379b5'
	      'c66f70cdcad216be0d36f596bd6af8bbfd1e04e5')

sha256sums=(SKIP
	dd4e7abf95eaf99bb8eb333a85dc5d2b1dac1399513da01183e52a07d8ac38d1  # scan_all.patch
	375fca3a458e547a12aa0eed54b4300218f5bee7f4182346ca32140c5acd6599  # glibc.patch
	bc03a618a0f264013ef8da35c4a9dfe05e4da627d2a271f95c0ecbe0be4eea1a  # usb_nodes.patch
	cc807ab7fd1c09f756370d75a039dc55ca3ad2134f1ea51bd1b4e33d64a4de8b  # config.h
	6e675fe5c8d8b7f9c7767cbf18d4d27858440816f12ff9d9fa53afcfa544cde5  # processdev
	cd5783ae78f96020887f1c64dbb3bb917ef9af134646c04351c4e4636271045d  # 00-modprobe
	eb561c3f2f271c52f47006866b2adde2955687aefae7c9f00ace3df7adf54195  # 99-remove_links
	32675f752e6204ec96d2aecf9821be9a6c0dd3fef98086d47466b79f7651dbb9  # initcpio.hook
	c69c014000bf2b0452592c51e288cdb1e1992d0634709f9fab70c061bb339b72  # initcpio.install
	fe732c2b2af7ea4f9bf4bd6ffb2d7e261247ff041fdb324bd9bf15cac41ccf12) # smdev.install

##  97a5ca7b4f276380a733f74b92e8526b5ed73cae57fd11b5c4fb91488f27669c  smdev-0.2.3-011-x86_64.pkg.tar.lz

