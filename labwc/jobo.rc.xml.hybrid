<?xml version="1.0"?>
<!--
  This is a very simple config file with many options missing. For a complete
  set of options with comments, see docs/rc.xml.all
-->
<labwc_config>

  <core>
    <decoration>server</decoration>
    <gap>0</gap>
    <adaptiveSync>no</adaptiveSync>
    <allowTearing>no</allowTearing>
    <reuseOutputMode>no</reuseOutputMode>
    <xwaylandPersistence>no</xwaylandPersistence>
  </core>

  <placement>
    <policy>center</policy>
    <!--
      When <placement><policy> is "cascade", the offset for cascading new
      windows can be overwritten like this:
      <cascadeOffset x="40" y="30" />
    -->
  </placement>

  <!-- <font><theme> can be defined without an attribute to set all places -->

  <theme>
    <name>Your theme choice</name>
    <cornerRadius>6</cornerRadius>
    <keepBorder>yes</keepBorder>
    <dropShadows>yes</dropShadows>
    <font place="ActiveWindow">
	<name>"Liberation Sans"</name> 
	<size="12"/>
      <slant>normal</slant>
      <weight>normal</weight>
    </font>
    <font place="InactiveWindow">
      <name>"Liberation Sans"</name>
      <size>10</size>
      <slant>normal</slant>
      <weight>normal</weight>
    </font>
    <font place="MenuItem">
      <name>"Liberation Sans"</name>
      <size>10</size>
      <slant>normal</slant>
      <weight>normal</weight>
    </font>
    <font place="OnScreenDisplay">
      <name>"Liberation Sans"</name>
      <size>10</size>
      <slant>normal</slant>
      <weight>normal</weight>
    </font>
  </theme>

  <!--
    Just as for window-rules, 'identifier' relates to app_id for native Wayland
    windows and WM_CLASS for XWayland clients.
  -->
  <windowSwitcher show="yes" preview="yes" outlines="yes" allWorkspaces="no">
    <fields>
      <field content="type" width="25%" />
      <field content="trimmed_identifier" width="25%" />
      <!-- <field content="identifier" width="25%" /> -->
      <field content="title" width="50%" />
    </fields>
  </windowSwitcher>

  <!--
    When using all workspaces option of window switcher, there are extra fields
    that can be used, workspace (variable length), state (single space),
    type_short (3 spaces), output (variable length), and can be set up
    like this. Note: output only shows if more than one output available.

    <windowSwitcher show="yes" preview="no" outlines="no" allWorkspaces="yes">
      <fields>
        <field content="workspace" width="5%" />
        <field content="state" width="3%" />
        <field content="type_short" width="3%" />
        <field content="output" width="9%" />
        <field content="identifier" width="30%" />
        <field content="title" width="50%" />
      </fields>
    </windowSwitcher>

    custom format - (introduced in 0.7.2)
    It allows one to replace all the above "fields" with one line, using a
    printf style format. For field explanations, "man 5 labwc-config".

    The example below would print "foobar",then type of window (wayland, X),
    then state of window (M/m/F), then output (shows if more than 1 active),
    then workspace name, then identifier/app-id, then the window title.
    Uses 100% of OSD window width.

    <windowSwitcher show="yes" preview="no" outlines="no" allWorkspaces="yes">
      <fields>
        <field content="custom" format="foobar %b %3s %-10o %-20W %-10i %t" width="100%" />
      </fields>
    </windowSwitcher>
  -->

  <!-- edge strength is in pixels -->
  <resistance>
    <screenEdgeStrength>20</screenEdgeStrength>
    <windowEdgeStrength>20</windowEdgeStrength>
    <unSnapThreshold>20</unSnapThreshold>
  </resistance>

  <resize>
    <!-- Show a simple resize and move indicator -->
    <popupShow>Never</popupShow>
    <!-- Let client redraw its contents while resizing -->
    <drawContents>yes</drawContents>
  </resize>

  <focus>
    <focusNew>no</focusNew>
    <!-- always try to focus new windows when they appear. other rules do
       apply -->
    <followMouse>no</followMouse>
    <!-- move focus to a window when you move the mouse into it -->
    <focusLast>no</focusLast>
    <!-- focus the last used window when changing desktops, instead of the one
       under the mouse pointer. when followMouse is enabled -->
    <underMouse>no</underMouse>
    <!-- move focus under the mouse, even when the mouse is not moving -->
    <focusDelay>200</focusDelay>
    <!-- when followMouse is enabled, the mouse must be inside the window for
       this many milliseconds (1000 = 1 sec) before moving focus to it -->
    <raiseOnFocus>no</raiseOnFocus>
    <!-- when followMouse is enabled, and a window is given focus by moving the
       mouse into it, also raise the window -->
  </focus>
  <snapping>
    <!-- Set range to 0 to disable window snapping completely -->
    <range>1</range>
    <overlay enabled="yes">
      <delay inner="500" outer="500" />
    </overlay>
    <topMaximize>yes</topMaximize>
    <notifyClient>always</notifyClient>
  </snapping>

  <placement>
    <policy>Smart</policy>
    <!-- 'Smart' or 'UnderMouse' -->
    <center>no</center>
    <!-- whether to place windows in the center of the free area found or
       the top left corner -->
    <monitor>Primary</monitor>
    <!-- with Smart placement on a multi-monitor system, try to place new windows
       on: 'Any' - any monitor, 'Mouse' - where the mouse is, 'Active' - where
       the active window is, 'Primary' - only on the primary monitor -->
    <primaryMonitor>Active</primaryMonitor>
    <!-- The monitor where labwc should place popup dialogs such as the
       focus cycling popup, or the desktop switch popup.  It can be an index
       from 1, specifying a particular monitor.  Or it can be one of the
       following: 'Mouse' - where the mouse is, or
                  'Active' - where the active window is -->
  </placement>
  <desktops>
    <popupTime>3910</popupTime>

    <!-- this stuff is only used at startup, pagers allow you to change them
       during a session
       these are default values to use when other ones are not already set
       by other applications, or saved in your session
       use obconf if you want to change these without having to log out
       and back in -->
    <number>4</number>
    <firstdesk>1</firstdesk>
    <names>
      <name>jobo 1</name>
      <name>jobo 2</name>
      <name>jobo 3</name>
      <name>jobo 4</name>
    </names>
    <!-- The number of milliseconds to show the popup for when switching
       desktops.  Set this to 0 to disable the popup. -->
  </desktops>
  <keyboard>
    <default />
    <!-- Use a different terminal emulator -->
    <keybind key="C-A-t">
	  <action name="Execute" command="foot" />
    </keybind>
    <keybind key="W-Return">
      <action name="Execute" command="foot" />
    </keybind>
    <!--
      Remove a previously defined keybind
      A shorter alternative is <keybind key="W-F4" />
    -->
    <keybind key="W-F4">
      <action name="None" />
    </keybind>
  </keyboard>
  <mouse>
    <default />
    <!-- Show a custom menu on desktop right click -->
    <context name="Root">
      <mousebind button="Right" action="Press">
        <action name="ShowMenu">
          <menu>root-menu</menu>
        </action>
      </mousebind>
    </context>
  </mouse>
  <applications>
    <!--
  # this is an example with comments through out. use these to make your
  # own rules, but without the comments of course.
  # you may use one or more of the name/class/role/title/type rules to specify
  # windows to match
  <application name="the window's _OB_APP_NAME property (see obxprop)"
              class="the window's _OB_APP_CLASS property (see obxprop)"
          groupname="the window's _OB_APP_GROUP_NAME property (see obxprop)"
         groupclass="the window's _OB_APP_GROUP_CLASS property (see obxprop)"
               role="the window's _OB_APP_ROLE property (see obxprop)"
              title="the window's _OB_APP_TITLE property (see obxprop)"
               type="the window's _OB_APP_TYPE property (see obxprob)..
                      (if unspecified, then it is 'dialog' for child windows)">
  # you may set only one of name/class/role/title/type, or you may use more
  # than one together to restrict your matches.
  # the name, class, role, and title use simple wildcard matching such as those
  # used by a shell. you can use * to match any characters and ? to match
  # any single character.
  # the type is one of: normal, dialog, splash, utility, menu, toolbar, dock,
  #    or desktop
  # when multiple rules match a window, they will all be applied, in the
  # order that they appear in this list
    # each rule element can be left out or set to 'default' to specify to not 
    # change that attribute of the window
    <decor>yes</decor>
    # enable or disable window decorations
    <shade>no</shade>
    # make the window shaded when it appears, or not
    <position force="no">
      # the position is only used if both an x and y coordinate are provided
      # (and not set to 'default')
      # when force is "yes", then the window will be placed here even if it
      # says you want it placed elsewhere.  this is to override buggy
      # applications who refuse to behave
      <x>center</x>
      # a number like 50, or 'center' to center on screen. use a negative number
      # to start from the right (or bottom for <y>), ie -50 is 50 pixels from
      # the right edge (or bottom). use 'default' to specify using value
      # provided by the application, or chosen by labwc, instead.
      <y>200</y>
      <monitor>1</monitor>
      # specifies the monitor in a xinerama setup.
      # 1 is the first head, or 'mouse' for wherever the mouse is
    </position>
    <size>
      # the size to make the window.
      <width>20</width>
      # a number like 20, or 'default' to use the size given by the application.
      # you can use fractions such as 1/2 or percentages such as 75% in which
      # case the value is relative to the size of the monitor that the window
      # appears on.
      <height>30%</height>
    </size>
    <focus>yes</focus>
    # if the window should try be given focus when it appears. if this is set
    # to yes it doesn't guarantee the window will be given focus. some
    # restrictions may apply, but labwc will try to
    <desktop>1</desktop>
    # 1 is the first desktop, 'all' for all desktops
    <layer>normal</layer>
    # 'above', 'normal', or 'below'
    <iconic>no</iconic>
    # make the window iconified when it appears, or not
    <skip_pager>no</skip_pager>
    # asks to not be shown in pagers
    <skip_taskbar>no</skip_taskbar>
    # asks to not be shown in taskbars. window cycling actions will also
    # skip past such windows
    <fullscreen>yes</fullscreen>
    # make the window in fullscreen mode when it appears
    <maximized>true</maximized>
    # 'Horizontal', 'Vertical' or boolean (yes/no)
  </application>
  # end of the example
-->
  </applications>

  <!--
    A touch configuration can be bound to a specific device. If device
    name is left empty, the touch configuration applies to all touch
    devices or functions as a fallback. Multiple touch configurations
    can exist.
    See the libinput device section for obtaining the device names.

    Direct cursor movement to a specified output. If the compositor is
    running in nested mode, this does not take effect.
  -->
  <touch deviceName="" mapToOutput="" />

  <!--
    The tablet cursor movement can be restricted to a single output.
    If output is left empty or the output does not exists, the tablet
    will span all outputs.

    The tablet orientation can be changed in 90 degree steps, thus
    *rotate* can be set to [0|90|180|270]. Rotation will be applied
    after applying tablet area transformation.

    The active tablet area can be specified by setting the *top*/*left*
    coordinate (in mm) and/or *width*/*height* (in mm). If width or
    height are omitted or default (0.0), width/height will be set to
    the remaining width/height seen from top/left.

    The tablet can be forced to always use mouse emulation. This prevents
    tablet specific restrictions, e.g. no support for drag&drop, but also
    omits tablet specific features like reporting pen pressure.

    Pen buttons emulate regular mouse buttons. The pen *button* can be any
    of [Stylus|Stylus2|Stylus3] and can be mapped to mouse buttons
    [Right|Middle|Side]. Tablet pad buttons [Pad|Pad2|Pad3|..|Pad9] also
    emulate regular mouse buttons and can be mapped to any mouse button.
    When using mouse emulation, the pen tip [tip] and the stylus buttons
    can be set to any available mouse button [Left|Right|Middle|..|Task].
  -->
  <tablet mapToOutput="" rotate="0" mouseEmulation="no">
    <!-- Active area dimensions are in mm -->
    <area top="0.0" left="0.0" width="0.0" height="0.0" />
    <map button="Tip" to="Left" />
    <map button="Stylus" to="Right" />
    <map button="Stylus2" to="Middle" />
  </tablet>

  <!--
    All tablet tools, except of type mouse and lens, use absolute
    positioning by default. The *motion* attribute allows to set tools
    to relative motion instead. When using relative motion,
    *relativeMotionSensitivity* controls the speed of the cursor. Using
    a value lower than 1.0 decreases the speed, using a value greater than
    1.0 increases the speed of the cursor.
  -->
  <tabletTool motion="absolute" relativeMotionSensitivity="1.0" />

  <!--
    The *category* attribute is optional and can be set to touch, touchpad,
    non-touch, default or the name of a device. You can obtain device names by
    running *libinput list-devices* as root or member of the input group.

    Tap is set to *yes* by default. All others are left blank in order to use
    device defaults.

    All values are [yes|no] except for:
      - pointerSpeed [-1.0 to 1.0]
      - accelProfile [flat|adaptive]
      - tapButtonMap [lrm|lmr]
      - clickMethod [none|buttonAreas|clickfinger]
      - sendEventsMode [yes|no|disabledOnExternalMouse]
      - calibrationMatrix [six float values split by space]
  -->
  <libinput>
    <device>
      <naturalScroll></naturalScroll>
      <leftHanded></leftHanded>
      <pointerSpeed></pointerSpeed>
      <accelProfile></accelProfile>
      <tap>yes</tap>
      <tapButtonMap></tapButtonMap>
      <tapAndDrag></tapAndDrag>
      <dragLock></dragLock>
      <middleEmulation></middleEmulation>
      <disableWhileTyping></disableWhileTyping>
      <clickMethod></clickMethod>
      <sendEventsMode></sendEventsMode>
      <calibrationMatrix></calibrationMatrix>
  
    </device>
  </libinput> -->

  <!--
    # Window Rules
    #   - Criteria can consist of 'identifier', 'title', 'sandboxEngine' or
    #     'sandboxAppId'. AND logic is used when multiple options are specified.
    #   - 'identifier' relates to app_id for native Wayland windows and
    #     WM_CLASS for XWayland clients.
    #   - Criteria can also contain `matchOnce="true"` meaning that the rule
    #     must only apply to the first instance of the window with that
    #     particular 'identifier' or 'title'.
    #   - Matching against patterns with '*' (wildcard) and '?' (joker) is
    #     supported. Pattern matching is case-insensitive.

    <windowRules>
      <windowRule identifier="*"><action name="Maximize"/></windowRule>
      <windowRule identifier="foo" serverDecoration="yes"/>
      <windowRule title="bar" serverDecoration="yes"/>
      <windowRule identifier="baz" title="quax" serverDecoration="yes"/>
    </windowRules>

    # Example below for `lxqt-panel` and `pcmanfm-qt \-\-desktop`
    # where 'matchOnce' is used to avoid applying rule to the panel
    # configuration window with the same 'app_id'.

    <windowRules>
      <windowRule identifier="lxqt-panel" matchOnce="true">
        <skipTaskbar>yes</skipTaskbar>
        <action name="MoveTo" x="0" y="0" />
        <action name="ToggleAlwaysOnTop"/>
      </windowRule>
      <windowRule title="pcmanfm-desktop*">
        <skipTaskbar>yes</skipTaskbar>
        <skipWindowSwitcher>yes</skipWindowSwitcher>
        <fixedPosition>yes</fixedPosition>
        <action name="MoveTo" x="0" y="0" />
        <action name="ToggleAlwaysOnBottom"/>
      </windowRule>
      <windowRule identifier="org.qutebrowser.qutebrowser">
        <action name="ResizeTo" width="1024" height="800" />
        <action name="AutoPlace"/>
      </windowRule>
    </windowRules>
  -->

  <menu>
    <ignoreButtonReleasePeriod>250</ignoreButtonReleasePeriod>
  </menu>

  <!--
    Magnifier settings
    'width' sets the width in pixels of the magnifier window.
    'height' sets the height in pixels of the magnifier window.
    'initScale' sets the initial magnification factor at boot.
    'increment' sets the amount by which the magnification factor
      changes when 'ZoomIn' or 'ZoomOut' are called.
    'useFilter' sets whether to use a bilinear filter on the magnified
      output or simply to take nearest pixel.
  -->
  <magnifier>
    <width>400</width>
    <height>400</height>
    <initScale>2.0</initScale>
    <increment>0.2</increment>
    <useFilter>true</useFilter>
  </magnifier>

</labwc_config>
