# This file contains all themerc options with default values
#
# System-wide and local themes can be overridden by creating a copy of this
# file and renaming it to $HOME/.config/labwc/themerc-override. Be careful
# though - if you only want to override a small number of specific options,
# make sure all other lines are commented out or deleted.

# general
border.width: 1

#
# The global padding.{width,height} of openbox are not supported because
# the default labwc button geometry deviates from that of openbox
#
window.titlebar.padding.width: 0
window.titlebar.padding.height: 0

# window border
window.active.border.color: #f2d6a0
window.inactive.border.color: #f9e314

# ToggleKeybinds status indicator
window.active.indicator.toggled-keybind.color: #ff0000

# window titlebar background
window.active.title.bg.color: #faab12
window.inactive.title.bg.color: #bb9a49

# window titlebar text
window.active.label.text.color: #131b1a
window.inactive.label.text.color: #000000
window.label.text.justify: center

# window button width
window.button.width: 23
window.button.height: 23
window.button.spacing: 0

# window button hover effect
window.button.hover.bg.corner-radius: 0

# window buttons
window.active.button.unpressed.image.color: #000000
window.inactive.button.unpressed.image.color: #000000

# window drop-shadows
window.active.shadow.size: 60
window.inactive.shadow.size: 40
window.active.shadow.color: #00000060
window.inactive.shadow.color: #00000040

# Note that "menu", "iconify", "max", "close" buttons colors can be defined
# individually by inserting the type after the button node, for example:
#
#     window.active.button.iconify.unpressed.image.color: #333333

# menu
menu.overlap.x: 2
menu.overlap.y: 2
menu.width.min: 10
menu.width.max: 220
menu.border.width: 1
menu.border.color: #a2a3a4
menu.items.bg.color: #2c3b4a
menu.items.text.color: #f4f0f3
menu.items.active.bg.color: #fcda33
menu.items.active.text.color: #1f1d13
menu.items.padding.x: 6
menu.items.padding.y: 2
menu.separator.width: 34
menu.separator.padding.width: 6
# menu.separator.padding.height: 3
menu.separator.color: #e1640f
menu.title.bg.color: #F1634F
menu.title.text.color: #030409
menu.title.text.justify: Center

# on screen display (window-cycle dialog)
osd.bg.color: #112231
osd.border.color: #E4D211
osd.border.width: 4
osd.label.text.color: #A6C3F6

# width can be set as percent (of screen width)
# example 50% or 75% instead of 600, max 100%
osd.window-switcher.width: 300
osd.window-switcher.radius: 12
osd.window-switcher.padding: 4
osd.window-switcher.item.padding.x: 10
osd.window-switcher.item.padding.y: 1
osd.window-switcher.item.active.border.width: 2
osd.window-switcher.preview.border.width: 2
osd.window-switcher.preview.border.color: #dddda6,#000000,#dddda6

osd.workspace-switcher.boxes.width: 20
osd.workspace-switcher.boxes.height: 20

# Default values for following options change depending on the rendering
# backend. For software-based renderers, *.bg.enabled is "no" and
# *.border.enabled is "yes" if not set. For hardware-based renderers,
# *.bg.enabled is "yes" and *.border.enabled is "no" if not set.
# Setting *.bg.enabled to "yes" for software-based renderer with translucent
# background color may severely impact performance.
#
# snapping.overlay.region.bg.enabled:
# snapping.overlay.edge.bg.enabled:
# snapping.overlay.region.border.enabled:
# snapping.overlay.edge.border.enabled:

snapping.overlay.region.bg.color: #8080b380
snapping.overlay.edge.bg.color: #8080b380
snapping.overlay.region.border.width: 1
snapping.overlay.edge.border.width: 1
snapping.overlay.region.border.color: #dddda6,#000000,#dddda6
snapping.overlay.edge.border.color: #dddda6,#000000,#dddda6
