#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=pokerth
pkgver=1.1.2
pkgrel=051
pkgdesc="Client to online Poker game written in C++/QT"
url="http://www.pokerth.net/"
depends=('curl' 'boost-libs=1.86.0' 'gsasl' 'protobuf'
         'qt5-base' 'sdl_mixer' 'tinyxml')

makedepends=('boost=1.86.0')

source=(https://sourceforge.net/projects/$pkgname/files/$pkgname/$pkgver/$pkgname-$pkgver.tar.gz
        ${pkgname}-${pkgver}.patch
        ${pkgname}-${pkgver}.patch.2019
        ${pkgname}-${pkgver}.patch.2020
        ${pkgname}-${pkgver}.patch.2023
        ${pkgname}-${pkgver}.patch.2023.xdg
        ${pkgname}-${pkgver}.patch.2024.boost_deps)

prepare() {
  cd "$srcdir/$pkgname-$pkgver-rc"

  # ---< required for v1.1.2 >--------------------------------------------------
  # these changes should be incorporated in next release ~feb-2018
  patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch"
  # ----------------------------------------------------------------------------
  # changes to permit building with boost 1.70
  patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch.2019"
  # ----------------------------------------------------------------------------
  # change to permit building with boost 1.74
  # see also DEFINE+="BOOST_BIND_GLOBAL_PLACEHOLDERS" in build below
  patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch.2020"
  # ----------------------------------------------------------------------------
  # change to explicitly link libabsl_log_internal_message.so and
  #                           libabsl_log_internal_check_op.so
  #                           patch revised by xx55tt
  # change to explicitly link libabsl_log_internal_nullguard
  #                           patch revised by jiocash 2024/12
  patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch.2023"
  # ----------------------------------------------------------------------------
  # change to use XDG_CONFIG_HOME if available
  # changes suggested by @viktoracoric
  # patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch.2023.xdg"
  # ----------------------------------------------------------------------------
  # change to adapt to boost deprecation removals in 1.85
  # corrections taken from upstream
  # adapts to BOOST_VERSION: should work with either 1.83 or 1.86
  patch -Np1 -i "${srcdir}/pokerth-1.1.2.patch.2024.boost_deps"
  # ----------------------------------------------------------------------------

  # good idea to do this at all times
  protoc -I=$srcdir/$pkgname-$pkgver-rc/ --cpp_out=$srcdir/$pkgname-$pkgver-rc/src/third_party/protobuf/ $srcdir/$pkgname-$pkgver-rc/pokerth.proto $srcdir/$pkgname-$pkgver-rc/chatcleaner.proto

}

build() {
  cd "$srcdir/$pkgname-$pkgver-rc"

  # QMAKE_CFLAGS_ISYSTEM workaround to prevent generation of "-isystem /usr/include"
  qmake CONFIG+="client" DEFINES+="BOOST_BIND_GLOBAL_PLACEHOLDERS" QMAKE_CFLAGS_ISYSTEM= -spec linux-g++ ${pkgname}.pro
  make
}

package() {
  cd "$srcdir/$pkgname-$pkgver-rc"

  make INSTALL_ROOT="$pkgdir" install

  install -D pokerth "$pkgdir/usr/bin/pokerth"
  # added for icons 2024-06-26 Mailaender 
  install -Dm644 pokerth.png "$pkgdir/usr/share/icons/hicolor/128x128/apps/pokerth.png"
  install -Dm644 pokerth.svg "$pkgdir/usr/share/icons/hicolor/scalable/apps/pokerth.svg"
  install -D -m644 docs/pokerth.1 "$pkgdir/usr/share/man/man1/pokerth.1"
  install -D -m644 data/data-copyright.txt "$pkgdir/usr/share/licenses/pokerth/data-copyright.txt"
  rm -f "$pkgdir/usr/share/pokerth/data/data-copyright.txt"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL' 'custom')

md5sums=('8fd7d7fc7ece17315e58aa3240dd4586'
         '0ef5541fc6008dfb2521dcab47afb659'
         '50d427bd8afc57fb61e186de6c4e5601'
         '5bdcc1f2240c20f4b766b183d93836b3'
         'a9b886e89d5c262c95c1d90e459d519c'
         'e61eae14e6394f4745245e2ef42d812c'
         '5a563f6b3b144b0e6c2eb4bacd4ca3d4')

sha256sums=(02cb3dee8077de20cd4491b66049b123d8bdd7fa243855f97c817934875c297a  # pokerth-1.1.2.tar.gz
	82827c49650aa64c4d2ed70e9e3a0664bee3895ffe534bf1b1caa635b9eed11b  # pokerth-1.1.2.patch
	e921e07d0833300b2a8611594259e26ce0ae259d0276539e759ddef6892f5add  # pokerth-1.1.2.patch.2019
	236aab80b19232f1431f251915618f1a7b9e719f75e0d05fdae18ad365ffc0e8  # pokerth-1.1.2.patch.2020
	28949d11b24f08ba6406f5df49ecbd84d715c0cfa70e657452e9ce270a86e8d6  # pokerth-1.1.2.patch.2023
	00a78017bb80e81c65eea8acf1fb46b70e075b91d4aba427e008f53f3c457a1a  # pokerth-1.1.2.patch.xdg.2023
	87a89d3050b65dc0f65c5e472c88607ab808cd690373b263bf9ea05683fb3e63) # pokerth-1.1.2.patch.2024.boost_deps

##  208cd771502f31bdc4c0e9b2a04e9044fc5895727362cc1026b6cc2f5b5da6fc  pokerth-1.1.2-051-x86_64.pkg.tar.lz
