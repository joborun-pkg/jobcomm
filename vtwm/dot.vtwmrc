#
# system.vtwmrc.3D
# 
# Default VTWM configuration file; should be kept small to conserve string  
# space in systems whose compilers don't handle medium-sized strings.
#
# Sites should tailor this file, providing any extra title buttons, menus, 
# etc., that may be appropriate for their environment.  For example, if most    
# of the users were accustomed to uwm, the defaults could be set up not to 
# decorate any windows and to use meta-keys.  
#
#
# Variables
#

ShowIconManager
#NoGrabServer
RestartPreviousState
NoDefaults
"Focus"		f.focus
#"Unfocus"		f.unfocus

ShowIconManager
SortIconManager
IconManagerGeometry  "1720x90+1+1" 13
IconifyByUnmapping

RightHandSidePulldownMenus

#NaturalAutoPanBehavior
#NotVirtualGeometries
FixManagedVirtualGeometries
FixTransientVirtualGeometries

ButtonColorIsFrame
ShallowReliefWindowButton

MoveDelta			4

#ResizeRegion			"NorthEast"
ResizeRegion			"SouthWest"

FramePadding			2
ButtonIndent			2
TitlePadding			0
TitleButtonBorderWidth		2

BorderWidth			3
BorderBevelWidth		1
ButtonBevelWidth		1
DoorBevelWidth			1
IconBevelWidth			1
IconManagerBevelWidth		1
InfoBevelWidth			2
MenuBevelWidth			3
TitleBevelWidth			3
VirtualDesktopBevelWidth	1

ClearBevelContrast		20
DarkBevelContrast		80

VirtualDesktop			"3x3-2-2" 20
PanDistanceX			100
PanDistanceY			100
PanResistance			930
AutoPan					100
# TrueType   Bitstream Vera Sans
EnableXftFontRenderer
DefaultFont		"Liberation Sans:bold:roman:size=12:antialias=true"
DoorFont		"Liberation Sans:regular:roman:size=8:antialias=true"
IconFont		"Liberation Sans:regular:roman:size=11:antialias=true"
IconManagerFont	"Liberation Sans:regular:roman:size=11:antialias=true"
InfoFont		"Liberation Sans:regular:roman:size=14:antialias=true"
MenuFont		"Liberation Sans:regular:roman:size=12:antialias=true"
MenuTitleFont	"Liberation Sans:regular:roman:size=13:antialias=true"
ResizeFont		"Liberation Sans:regular:roman:size=12:antialias=true"
TitleFont		"Liberation Sans:bold:roman:size=10:antialias=true"
VirtualDesktopFont	"Liberation Sans:regular:roman:size=11:antialias=true"

#
# Lists
#

NoHighlite
{
	"lxterminal"
	"browser1"
	"browser2"
	"librewolf"
}

SqueezeTitle

WarpCursor

NailedDown
{
	"VTWM *"
	"xclock"
	"xload"
}

NoTitle
{
	"VTWM *"
	"xclock"
	"xload"
}

DontShowInDisplay
{
	"VTWM *"
	"xclock"
	"xload"
}

IconManagerDontShow
{
	"VTWM *"
	"xclock"
	"xload"
}

Pixmaps
{
	TitleHighlight		":xpm:sunkenlines"
	MenuIconPixmap		":xpm:rarrow"
	IconManagerPixmap		":xpm:box"
}

Color
{
	DefaultBackground		"#242122"
	DefaultForeground		"gray35"
	BorderColor			"gray77"
				{
					"lxterminal" "orange"
					"browser1" "gold"
					"browser2" "blue"
					"thunderbird" "purple"
					"sakura" "red"
				}
	BorderTileBackground		"gray80"
	BorderTileForeground		"gray80"
	DoorBackground		"#363636"
	DoorForeground		"orange"
	TitleBackground		"#262626"
	TitleForeground		"gray85"
	MenuBackground		"#262626"
	MenuForeground		"gray85"
	MenuTitleBackground		"gray30"
	MenuTitleForeground		"black"
	IconBackground		"#262626"
	IconForeground		"gray35"
	IconBorderColor		"gold"
	IconManagerBackground		"#222426"
	IconManagerForeground		"orange"
	VirtualBackground		"gray60"
	VirtualForeground		"gray90"
	DesktopDisplayBackground	"#CC0033"
	DesktopDisplayForeground	"#F2F2F2"
	RealScreenBackground        	"gray98" 
	RealScreenForeground        	"gray68" 
}

#
# Functions and Bindings
#
 
 "Left" = c : all : f.panleft "100"
 "Right" = c : all : f.panright "100"
 "Up" = c : all : f.panup "100"
 "Down" = c : all : f.pandown "100"
 "Tab" = a : all : f.forwiconmgr
 "Tab" = m : all : f.backiconmgr
# "Shift" = a : all : f.forwiconmgr
# "Shift" = m : all : f.backiconmgr

Function "move-or-iconify" { f.move f.deltastop f.iconify }
Function "move-or-raiselower" { f.move f.deltastop f.raiselower }
Function "warpwindow"         { f.warpsnug f.warp f.warpsnug }

LeftTitleButton ":xpm:darrow" = f.menu "arrange"
RightTitleButton ":xpm:dot" = f.iconify
RightTitleButton ":xpm:resize" = f.resize

Button1 = : root : f.menu "main1"
Button2 = : root : f.restart
Button3 = : root : f.menu "main2"

Button1 = : title : f.function "move-or-raiselower"
Button2 = : title : f.resize
Button3 = : title : f.raiselower

Button1 = : frame : f.resize
Button2 = : frame : f.move
Button3 = : frame : f.iconify

Button1 = : door : f.enterdoor
Button2 = : door : f.deletedoor
Button3 = : door : f.namedoor

Button1 = : icon : f.function "move-or-iconify"
Button3 = : icon : f.menu "arrange"

Button1 = : iconmgr : f.iconify
Button3 = : iconmgr : f.warp

Button1 = : virtual : f.movescreen
Button2 = : virtual : f.movescreen
Button3 = : virtual : f.movescreen

Button1 = : desktop : f.movescreen
Button2 = : desktop : f.warp                  # morph desktop to window
Button3 = : desktop : f.function "warpwindow" # morph window to desktop

#
# Menus
#

menu "main1"
{
	"  VTWM L "		f.title
	""		f.separator
	"Ops"		f.menu "ops"
	"Arrange"		f.menu "arrange"
	"US"		f.exec "setxkbmap us &"
	"lang-choice"	f.exec "setxkbmap es &"
	"exec"		f.exec "gmrun &"
	"lxterm"		f.exec "lxterminal &"
	"sakura"		f.exec "sakura &"
	"browser1"		f.exec "browser1 &"
	"librewolf"		f.exec "librewolf --nodbus &"
	"leafpad"		f.exec "leafpad &"
	"pcmanfm"		f.exec "pcmanfm &"
	"bg"		f.exec "feh --bg-tile ~/bgtile.jpg &"
	"conky"		f.exec "conky -c ~/.conkyrc &"

	"kill conky"	f.exec "killall conky &"
	""		f.separator
	"Applications"	f.menu "apps"
	""		f.separator
##	"some-script"	f.exec "sh ~/some-script.sh &"
	"Restart"		f.restart
	"Exit"		f.quit}

menu "main2"
{
	"  VTWM-R "		f.title
	""		f.separator
	"exec"		f.exec "gmrun &"
	"lxterm"		f.exec "lxterminal &"
	"sakura"		f.exec "sakura &"
	"librewolf"		f.exec "librewolf --nodbus &"
	"leafpad"		f.exec "leafpad &"
	"pcmanfm"		f.exec "pcmanfm &"
#	"bg leaves"		f.exec "feh --bg-fill /usr/share/bg/leaves-autumn-dark-branches.jpg &"
	"bg tiles"		f.exec "feh --bg-tile ~/bgtile.jpg &"
	"conky"		f.exec "conky -c ~/.conkyrc &"
	"kill conky"	f.exec "killall conky &"
	""		f.separator
	"New Door"		f.newdoor
	"Applications"	f.menu "apps"
	""		f.separator
	"24"		f.exec "sh ~/24.sh &"
	"30+24"		f.exec "sh ~/30+24.sh &"
	"Restart"		f.restart
	"Exit"		f.quit
}

menu "apps"
{
	"  Apps  "		f.title
	"Ops"		f.menu "ops"
	"Arrange"		f.menu "arrange"
	"exec"		f.exec "gmrun &"
	"lxterm"		f.exec "lxterminal &"
	"sakura"		f.exec "sakura &"
	"librewolf"		f.exec "librewolf --nodbus &"
	"leafpad"		f.exec "leafpad &"
	"pcmanfm"		f.exec "pcmanfm &"
	"bg"		f.exec "feh --bg-tile ~/bgtile.jpg &"
	"conky"		f.exec "conky -c ~/.conkyrc &"
}
menu "ops"
{
	"  Operations  "	f.title
	"Auto Pan"		f.autopan
	"New Door"		f.newdoor
	"Snap Screen"	f.snaprealscreen
	"Static Icons"	f.staticiconpositions
	"Warp Snug"		f.warpsnug
	"Warp Visible"	f.warpvisible
	""		f.separator
	"Show Icon Mgr"	f.showiconmgr
	"Hide Icon Mgr"	f.hideiconmgr
	"Show Desktop"	f.showdesktopdisplay
	"Hide Desktop"	f.hidedesktopdisplay
	""		f.separator
	"Refresh"		f.refresh
}

menu "arrange"
{
	"  Arrange  "	f.title
	"Autoraise"		f.autoraise
	"Raise"		f.raise
	"Lower"		f.lower
	"Nail"		f.nail
	""		f.separator
	"Move"		f.move
	"Size"		f.resize
	"Iconify"		f.iconify
	""		f.separator
	"Full Zoom"		f.fullzoom
	"Horiz Zoom"	f.horizoom
	"Vert Zoom"		f.zoom
	""		f.separator
	"Left Title"	f.squeezeleft
	"Center Title"	f.squeezecenter
	"Right Title"	f.squeezeright
	""		f.separator
	"Identify"		f.identify
	""		f.separator
	"Delete"		f.delete
	"Destroy"		f.destroy
}
