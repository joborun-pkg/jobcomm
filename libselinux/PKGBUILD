#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=libselinux
pkgver=3.8
pkgrel=02
pkgdesc="SELinux library and simple utilities"
url='https://github.com/SELinuxProject/selinux'
groups=('selinux')
depends=('libsepol>=3.7' 'pcre2')
makedepends=('pkgconf' 'python' 'python-pip' 'python-setuptools' 'ruby' 'xz' 'swig')
optdepends=('python: python bindings'
            'ruby: ruby bindings')
conflicts=("selinux-usr-${pkgname}")
provides=("selinux-usr-${pkgname}=${pkgver}-${pkgrel}")
source=("https://github.com/SELinuxProject/selinux/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz" # {,.asc} NEW KEY NOT FOUND YET
#	0001-libselinux-fix-swig-bindings-for-4.3.0.patch
	"libselinux.tmpfiles.d")

prepare() {
  cd "${pkgname}-${pkgver}"

  # Apply https://github.com/SELinuxProject/selinux/commit/8e0e718bae53fff30831b92cd784151d475a20da
  # libselinux: fix swig bindings for 4.3.0
#  patch -Np2 -i "../0001-libselinux-fix-swig-bindings-for-4.3.0.patch"
}


build() {
  cd "${pkgname}-${pkgver}"

  # Do not build deprecated rpm_execcon() interface. It is useless on Arch Linux anyway.
  export DISABLE_RPM=y

  # Use pcre2 explicitely even though it is the default since
  # https://github.com/SELinuxProject/selinux/commit/e0da140d82c0ebebf1060ce87d0f11276c7fc59a
  export USE_PCRE2=y

  export CFLAGS="${CFLAGS} -fno-semantic-interposition"
  make swigify
  make all
  make PYTHON=/usr/bin/python3 pywrap
  make RUBY=/usr/bin/ruby rubywrap
}

package() {
  cd "${pkgname}-${pkgver}"
  provides+=(libselinux.so)

  export DISABLE_RPM=y

  make DESTDIR="${pkgdir}" SBINDIR=/usr/bin SHLIBDIR=/usr/lib install
  make DESTDIR="${pkgdir}" PYTHON=/usr/bin/python3 SBINDIR=/usr/bin SHLIBDIR=/usr/lib install-pywrap
  make DESTDIR="${pkgdir}" RUBY=/usr/bin/ruby SBINDIR=/usr/bin SHLIBDIR=/usr/lib install-rubywrap
  /usr/bin/python3 -m compileall "${pkgdir}/$(/usr/bin/python3 -c 'from distutils.sysconfig import *; print(get_python_lib(plat_specific=1))')"

  install -Dm 0644 "${srcdir}"/libselinux.tmpfiles.d "${pkgdir}"/usr/lib/tmpfiles.d/libselinux.conf

  install -Dm 0644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)
#arch=('i686' 'x86_64' 'armv6h' 'aarch64')

license=('custom')

validpgpkeys=('B8682847764DF60DF52D992CBC3905F235179CF1'  # Petr Lautrbach <plautrba@redhat.com>
	  '63191CE94183098689CAB8DB7EF137EC935B0EAF')  # Jason Zaman <perfinion@gentoo.org>

sha256sums=(0c3756bca047c9270281d7c4dcdecd000b72e38a183c930661eba9690839b541  # libselinux-3.8.tar.gz
#	7f33102d9602cf9b52ee8fdadc76c279abad39d4ac81aebd23564cc52a8a6c36  # libselinux-3.8.tar.gz.asc
#	d176a9ec5f48decd8443b48403d03d24546f426cd1858f25270b489e06d29942  # 0001-libselinux-fix-swig-bindings-for-4.3.0.patch
	afe23890fb2e12e6756e5d81bad3c3da33f38a95d072731c0422fbeb0b1fa1fc) # libselinux.tmpfiles.d

##  6020727f7fd5513c294e7957f3f3fc87199ad0fb5d05455c8cff3f56c6a24ff2  libselinux-3.8-02-x86_64.pkg.tar.lz

