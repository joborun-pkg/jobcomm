#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=wayshot
#pkgname="$_pkgname-git"
pkgver=1.3.2.r1.edd1f149
pkgrel=02
pkgdesc="Screenshot tool for wlroots compositors"
url="https://git.sr.ht/~shinyzenith/$pkgname"
makedepends=(cargo git scdoc)
#optdepends=('slurp: for area selection')
provides=("$pkgname=${pkgver%%.r*}")
conflicts=("wayshot-git")
source=("git+$url")

prepare() {
	cd $pkgname

	# don't waste time zipping manpages
	rm -rfv build.rs

	export RUSTUP_TOOLCHAIN=stable
	cargo fetch --locked --target "$CARCH-unknown-linux-gnu"
}

pkgver() {
	cd $pkgname
	git blame -s -L"/^version =/,+1" Cargo.toml | awk '{
		ver = gensub(/[^0-9.]/, "", "g", $5);
		"git rev-list --count "$1"..HEAD" | getline commit_count;
		print ver".r"commit_count"."$1
	}'
}

build() {
	cd $pkgname
	export RUSTUP_TOOLCHAIN=stable
	export CARGO_TARGET_DIR=target
	cargo build --frozen --release --all-features
}

package() {
	cd $pkgname
	depends=(gcc-libs glibc slurp)
	install -vDm755 target/release/$pkgname -t "$pkgdir/usr/bin/"
	install -vDm644 docs/$pkgname.1.scd "$pkgdir/usr/share/man/man1/$pkgname.1"
	install -vDm644 docs/$pkgname.7.scd "$pkgdir/usr/share/man/man7/$pkgname.7"
	install -vDm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

validpgpkeys=('C18E2B48B8DA9B624C8B72D66DD485917B553B7B') # Shinyzenith#6969 (gpgkeypair) <aakashsensharma@gmail.com>

license=(BSD)

sha256sums=(SKIP)

##  aca4a6400adb6f029ad9f03be46fc7fc4d7b86e0f80d8c5f4d9fe16ea712e3e1  wayshot-1.3.2.r1.edd1f149-02-x86_64.pkg.tar.lz

