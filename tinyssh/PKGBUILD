#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=tinyssh
pkgver=20250201.r0.g372ce83
pkgrel=02
pkgdesc="Minimalistic SSH server using NaCl / TweetNaCl w/o systemd"
url="https://tinyssh.org/"
depends=('glibc')
makedepends=('git')
conflicts=('tinyssh-git' 'tinyssh-convert')
provides=("tinyssh=$pkgver" tinyssh-convert)
replaces=(tinyssh-convert)
source=("git+https://github.com/janmojzis/tinyssh.git"
        "tinyssh@.service::https://gitlab.archlinux.org/archlinux/packaging/packages/tinyssh/-/raw/main/tinyssh@.service"
        "tinyssh@.socket::https://gitlab.archlinux.org/archlinux/packaging/packages/tinyssh/-/raw/main/tinyssh@.socket"
        "tinysshgenkeys.service::https://gitlab.archlinux.org/archlinux/packaging/packages/tinyssh/-/raw/main/tinysshgenkeys.service"
	"tinyssh.66")

prepare() {
  cd $pkgname

  echo "/usr/bin" > "conf-bin"
}

pkgver() {
  cd $pkgname

  _tag=$(git tag -l --sort -v:refname | grep -E '^v?[0-9\.]+$' | head -n1)
  _rev=$(git rev-list --count $_tag..HEAD)
  _hash=$(git rev-parse --short HEAD)
  printf "%s.r%s.g%s" "$_tag" "$_rev" "$_hash" | sed 's/^v//'
}

build() {
  cd $pkgname

  make
}

package() {
  cd $pkgname

  make DESTDIR="$pkgdir" install

  install -d "$pkgdir/etc/tinyssh"

  mv "${pkgdir}"/usr/local/sbin "$pkgdir"/usr/bin
  mv "${pkgdir}"/usr/local/share "$pkgdir"/usr/share
  rmdir ${pkgdir}/usr/local

# provide the service files as reference for making runscripts for runit/s6 services
  install -Dm644 "$srcdir"/{tinysshgenkeys.service,tinyssh@.service,tinyssh@.socket,tinyssh.66} \
    -t "$pkgdir/usr/share/doc/$pkgname/"
  install -Dm644 $srcdir/$pkgname/README.md -t "$pkgdir/usr/share/doc/$pkgname/"
  install -Dm644 LICENCE.md -t "$pkgdir/usr/share/licenses/${pkgname}/"

  # Multi-call binary
  ln -sf /usr/bin/tinysshd "${pkgdir}"/usr/bin/tinysshd-makekey
  ln -sf /usr/bin/tinysshd "${pkgdir}"/usr/bin/tinysshd-printkey
  ln -sf /usr/bin/tinysshd "${pkgdir}"/usr/bin/tinysshnoneauthd
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('custom:Public Domain' 'custom:CC0')

sha256sums=(SKIP # tinyssh
	fad187a110e5bc042773128bab5dcd83b1250d57a163e5f1a22c7ed3f3f98fa1  # tinyssh@.service
	10d691b2137ce8b4203f19aaa57f44f9a9a23df1270190a284214e84ca3b4e38  # tinyssh@.socket
	ef21e31c9a015c7c16a1aec8cd8006069afd9850d536359bb1b8273320939f3b  # tinysshgenkeys.service
	1af009c8a99fac2304d55f52b5b4181a84568bb7d4d9dc80ba0fd919488d1a39) # tinyssh.66

##  5ea0ceb1dce59b874ac452d49df40eb68e10663d8a853a5c94a33df895c1945e  tinyssh-20250201.r0.g372ce83-02-x86_64.pkg.tar.lz

