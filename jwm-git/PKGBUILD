#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=jwm-git
_pkgname=jwm
pkgver=s1685.r158.56cebf9
pkgrel=02
pkgdesc="A light-weight window manager for the X11 Window System. Git version."
info_url="http://joewing.net/projects/jwm/"
url="https://github.com/joewing/jwm"
provides=('jwm')
makedepends=(git cairo gettext libjpeg-turbo libpng librsvg libx11
	libxext libxinerama libxmu libxpm libxrender pango)
backup=('etc/system.jwmrc')
conflicts=('jwm' 'jwm-snapshot' 'jwm-flashfixed' 'jwm-snapshot-lite')
source=("$_pkgname::git+$url.git"
        jwm.desktop)

pkgver() {
  cd $_pkgname
  # Use the tag of the last commit
  git describe --always | sed -r 's|-([^-]*)-g(.*)|.r\1.\2|'
}

prepare() {
  cd $srcdir/$_pkgname
  sh autogen.sh
  sed -i 's|/usr/local/share/|/usr/share/|'  example.jwmrc contrib/Makefile.in 
}

build() {
  cd $srcdir/$_pkgname
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc 
  make
}

package() {
   cd $srcdir/$_pkgname
   depends=(libx11 libjpeg-turbo libxext libxpm libxinerama libpng cairo librsvg libxmu libxrender pango)
   make BINDIR="$pkgdir/usr/bin" MANDIR="$pkgdir/usr/share/man" \
        DESTDIR="$pkgdir" SYSCONF="$pkgdir/etc" \
        mkdir_p="/usr/bin/mkdir -p" install
   install -Dm644 "$srcdir/jwm.desktop" "$pkgdir/usr/share/xsessions/jwm.desktop"
   install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
   install -Dm644 example.jwmrc -t "$pkgdir/usr/share/$pkgname/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('MIT')

md5sums=('SKIP'
         'ad898472f7538ffc3ff511c055fee535')

sha256sums=(SKIP # jwm.git
#	8bd68b1302991e9c45fbd6fe3d7d876ef0b37a9e831fc889cfdd38a7bf625793  # jwm 2.4.5
	489327c12bb44c6802144f972411dab7a44a61920c07028fa3d81cfd0ba9fd30) # jwm.desktop

##  9596798aa7f0d1db690ac596e026ec7a50aec4926a0f5df6847f100b3312fef7  jwm-git-s1685.r158.56cebf9-02-x86_64.pkg.tar.lz

