#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=gconf
pkgver=3.2.6+11+g07808097
pkgrel=014
pkgdesc="A configuration database system for gksu libgksu from old-Gnome"
url0="https://projects-old.gnome.org/gconf/"
url="https://gitlab.gnome.org/Archive/gconf"
makedepends=(git intltool gtk-doc gobject-introspection gnome-common glib2-devel
	automake autoconf gettext dbus-glib polkit)
install=gconf.install
_commit=0780809731c8ab1c364202b1900d3df106b28626 # The latest and last commit, dug out from deep within the waves of time...
source=("git+https://gitlab.gnome.org/Archive/gconf.git#commit=$_commit"
        01_xml-gettext-domain.patch gconf-reload.patch
        gconf-merge-schema gconfpkg gconf-{install,remove}.hook)

prepare() {
  cd $pkgname

  # Patch from fedora - reloads gconf after installing schemas
  patch -Np1 -i ../gconf-reload.patch

  # http://bugzilla.gnome.org/show_bug.cgi?id=568845
  patch -Np1 -i ../01_xml-gettext-domain.patch

  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd $pkgname
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib \
    --enable-defaults-service \
    --disable-gtk-doc \
    --disable-static \
    --disable-orbit \
    --disable-gsettings-backend \
    --without-dbus
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

check() {
  cd $pkgname
  make check
}

package() {
  depends=(libxml2 polkit libldap dbus-glib)

  DESTDIR="$pkgdir" make -C $pkgname install

  install -d "$pkgdir/etc/gconf/gconf.xml.system"
  install -D gconf-merge-schema gconfpkg -t "$pkgdir/usr/bin"
  install -Dm644 ./*.hook -t "$pkgdir/usr/share/libalpm/hooks"

  # fix dbus policy location - --with-dbusdir doesn't work
  install -Dm644 "$pkgdir/etc/dbus-1/system.d/org.gnome.GConf.Defaults.conf" -t "$pkgdir/usr/share/dbus-1/system.d"
  rm -rf "$pkgdir/etc/dbus-1/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('LGPL-2.0-only')

sha256sums=(9d4fdc49825c6938be59ae9f7e88eccc4fc8a1dc2c7ce130919916c32ccad179  # gconf 
	c883dec2b96978874a53700cfe7f26f24f8296767203e970bc6402b4b9945eb8  # 01_xml-gettext-domain.patch
	567b78d8b4b4bbcb77c5f134d57bc503c34867fcc6341c0b01716bcaa4a21694  # gconf-reload.patch
	ee6b6e6f4975dad13a8c45f1c1f0547a99373bdecdcd6604bfc12965c328a028  # gconf-merge-schema
	bf1928718caa5df2b9e54a13cfd0f15a8fe0e09e86b84385ce023616a114e898  # gconfpkg
	2732b2a6b187c5620105a036bde12edee99669605f70cbde56fe5f39619c3dc0  # gconf-install.hook
	436a65ff290095bc3d35d7d6297cf4d647f61e9f9922cea7ef9f1e251b447ff7) # gconf-remove.hook

##  77357a578486cf63044b91ca0f2f1c69789e80680fd2b522fa8ed84a41ff9b85  gconf-3.2.6+11+g07808097-014-x86_64.pkg.tar.lz

