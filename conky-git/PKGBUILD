#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=conky-git
pkgver=1.22.0.r10.g35c5a05
pkgrel=02
pkgdesc="Lightweight system monitor for X w/o systemd git rc version"
url="https://github.com/brndnmtthws/conky"
#replaces=('torsmo' 'conky' 'conky-lua' 'conky-lua-arch' 'conky-lua-archers-git' 'conky-cli')
provides=("conky=${pkgver}" 'conky-lua')
conflicts=('conky' 'conky11' 'conky-lua' 'conky-lua-arch' 'conky-lua-archers-git' 'conky-cli')
makedepends=(git cmake docbook2x docbook-xsl man-db glib2-devel gperf catch2-v2 
	pandoc-cli python-yaml python-jinja wayland-protocols gperf librsvg 
	wireless_tools libxdamage libxinerama wayland-protocols wayland imlib2)
source=("git+https://github.com/brndnmtthws/conky.git")

pkgver() {
  cd "conky"
  git describe --long --tags --abbrev=7 | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  cd "conky"
  # Unbundle catch2 to fix build with glibc 2.35
  rm -r tests/catch2
  ln -s /usr/include/catch2 tests

}

build() {
  local _flags=(
    -D CMAKE_CXX_FLAGS="$CXXFLAGS -ffat-lto-objects"
    -D BUILD_WEATHER_METAR=OFF
    -D MAINTAINER_MODE=OFF
    -D BUILD_TESTS=OFF
    -D BUILD_DOCS=ON
    -D BUILD_APCUPSD=OFF
    -D BUILD_ARGB=ON
    -D BUILD_EXTRAS=ON
    -D BUILD_WLAN=ON
    -D BUILD_XDBE=ON
    -D BUILD_XSHAPE=ON
    -D BUILD_HDDTEMP=OFF
    -D BUILD_I18N=OFF
    -D BUILD_IBM=OFF
    -D BUILD_HSV_GRADIENT=ON
    -D BUILD_IMLIB2=ON
    -D BUILD_CURL=ON
    -D BUILD_RSS=ON
    -D BUILD_NVIDIA=OFF
    -D BUILD_PULSEAUDIO=OFF
    -D BUILD_JOURNAL=OFF
    -D BUILD_WAYLAND=ON
    -D BUILD_IOSTATS=OFF
    -D BUILD_IPV6=OFF
    -D BUILD_LUA_CAIRO=ON
    -D BUILD_LUA_IMLIB2=ON
    -D BUILD_LUA_RSVG=ON
    -D BUILD_MOC=OFF
    -D BUILD_MPD=OFF
    -D BUILD_OLD_CONFIG=ON
    -D BUILD_PORT_MONITORS=OFF
    -D BUILD_X11=ON
    -D BUILD_XDAMAGE=ON
    -D BUILD_XDBE=ON
    -D BUILD_XFT=ON
    -D BUILD_XINERAMA=ON
    -D DEFAULTNETDEV=eth0
    -D OWN_WINDOW=ON
  )

  cmake -B build -S "conky" -Wno-dev \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    "${_flags[@]}"

  cmake --build build
}

check() {
  ctest --test-dir build --output-on-failure
}

package() {
  DESTDIR="${pkgdir}" cmake --install build

  cd "conky"

depends=(glibc lua  wireless_tools libxdamage libxinerama libxft imlib2 libxml2
    libpulse ncurses curl libncursesw.so libpulse.so libcurl.so)

  install -Dm644 extras/vim/syntax/conkyrc.vim.j2 -t \
        "$pkgdir"/usr/share/vim/vimfiles/syntax/
  install -Dm644 extras/vim/ftdetect/conkyrc.vim -t \
        "$pkgdir"/usr/share/vim/vimfiles/ftdetect/

  install -Dm644 COPYING -t "$pkgdir/usr/share/licenses/${pkgname}"

}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('BSD-3-Clause' 'GPL-3.0-or-later')

sha256sums=(SKIP)

##  50f5983731692a29eaf0c4da1596dc695f89c51da163bdff98f5e80e996309ce  conky-git-1.22.0.r10.g35c5a05-02-x86_64.pkg.tar.lz
