#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

pkgname=lokinet
pkgver=0.9.11
pkgrel=06
pkgdesc="Anonymous, decentralized and IP based overlay network for the internet w/o systemd spyware"
url="https://lokinet.org"
depends=('libuv' 'libsodium' 'curl' 'zeromq' 'unbound' 'jemalloc' 'spdlog' 'fmt')
makedepends=('cmake' 'git' 'python' 'nlohmann-json')
install='lokinet.install'
source=("https://github.com/oxen-io/lokinet/releases/download/v$pkgver/lokinet-v$pkgver.tar.xz"{,.sig}
        'lokinet.conf'
        'lokinet.sysusers'
        'lokinet.tmpfiles'
        'lokinet.rules'
        'fix-include.patch')

prepare() {
	patch --directory="lokinet-v$pkgver" --forward --strip=1 --input="${srcdir}/fix-include.patch"
}

build() {
	cd "lokinet-v$pkgver"

	rm -rf build && mkdir build && cd build

	# XXX cmake stuff overrides CFLAGS
	cmake \
		-DLOKINET_VERSIONTAG=release \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DNATIVE_BUILD=OFF \
		-DUSE_AVX2=OFF \
		-DWITH_TESTS=OFF \
		-DDOWNLOAD_SODIUM=OFF \
		-DSUBMODULE_CHECK=OFF \
		-DWITH_SYSTEMD=OFF \
		-DWITH_SETCAP=OFF \
		-DBUILD_LIBLOKINET=OFF \
		-DFORCE_OXENMQ_SUBMODULE=ON \
		-DFORCE_OXENC_SUBMODULE=ON \
		-DWITH_PEERSTATS_BACKEND=OFF \
		-DOXEN_LOGGING_FORCE_SUBMODULES=ON \
		-DOXEN_LOGGING_FMT_HEADER_ONLY=ON \
		-DOXEN_LOGGING_SPDLOG_HEADER_ONLY=ON \
		-Wno-dev \
		..
	make
}

package() {
	cd "lokinet-v$pkgver"
	install -D -m 644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
	cd build
	make DESTDIR="$pkgdir" install

#	install -D -m 644 "$srcdir/lokinet.service"                "$pkgdir/usr/lib/systemd/system/lokinet.service"
#	install -D -m 644 "$srcdir/lokinet-vpn@.service"           "$pkgdir/usr/lib/systemd/system/lokinet-vpn@.service"
#	install -D -m 644 "$srcdir/lokinet-resume.service"         "$pkgdir/usr/lib/systemd/system/lokinet-resume.service"
	install -D -m 644 "$srcdir/lokinet.sysusers"               "$pkgdir/usr/lib/sysusers.d/lokinet.conf"
	install -D -m 644 "$srcdir/lokinet.tmpfiles"               "$pkgdir/usr/lib/tmpfiles.d/lokinet.conf"
	install -D -m 750 -d "$pkgdir/usr/share/polkit-1/rules.d"
	install -D -m 644 "$srcdir/lokinet.rules"                  "$pkgdir/usr/share/polkit-1/rules.d/lokinet.rules"
	install -D -m 750 -d "$pkgdir/etc/loki"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL3')

validpgpkeys=('2CE6F2743138825B7A7E521D025C02EE3A092F2D') # Jeff Becker (probably not evil) <jeff@lokinet.io> https://lokinet.io/jeff.asc

sha256sums=(c16c82c7528beae5ceca072eaf6e1a9eb4b85247f1d60352344b5ee53009391c  # lokinet-v0.9.11.tar.xz
	163bbfb4874e538967c224d5b5c9c099bfbc1a37477c9de9f5d26e4cbd30aebd  # lokinet-v0.9.11.tar.xz.sig
	ff5e7db4e65463e50978da0185487bd4a7f213f04bdb6256e221089f833c6ab6  # lokinet.conf
	137cf7eeebc8737d62f3ccfad2398fb1c442a91cb9db7d650429b218dd949a00  # lokinet.sysusers
	b0d87fd610bbc7ef56148590dbc6ed6a68f36ec88db783f408a66c815705fd75  # lokinet.tmpfiles
	6ea4d917ce2e46b2c31af31b8c8c28054c5f977bab5b050c44e2029ab3248713  # lokinet.rules
	87b563eb9941120dc9d9999345fa3aebb21884237c4226fe8419aac0c3caf9b6) # fix-include.patch

##  d77024882b18cec8be14abca5b00700a173758fe41c394c583b207c85c1c7f06  lokinet-0.9.11-06-x86_64.pkg.tar.lz
