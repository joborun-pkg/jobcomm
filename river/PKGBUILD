#!/usr/bin/bash
# JOBoRun		: Jwm OpenBox Obarun RUNit
# Maintainer	: Joe Bo Run <joborun@disroot.org>
# PkgSource		: url="https://git.disroot.org/joborun-pkg/jobcomm/src/branch/main/$pkgname"
# Website		: https://pozol.eu
#-----------------------------------------| DESCRIPTION |---------------------------------------

## useful links
# https://codeberg.org/river/river
# https://github.com/riverwm/river

## options
: ${_build_xwayland:=true}
: ${_build_git:=true}

unset _pkgtype
[ "${_build_xwayland::1}" != "t" ] && _pkgtype+="-noxwayland"
[[ "${_build_git::1}" == "t" ]] && _pkgtype+="-git"

# basic info

pkgname="river"
#pkgname="$_pkgname${_pkgtype:-}"
#commit="543697847f2167152ae25f775f39541591b8d020 # refs/heads/master"
pkgver=0.3.7.r32.g5436978
pkgrel=03
pkgdesc='Dynamic tiling wayland compositor'
url='https://codeberg.org/river/river'
makedepends=(git scdoc zig xorg-xwayland wayland-protocols
	wlroots vulkan-icd-loader seatd lcms2
	libevdev  libxkbcommon)
[[ "${_build_xwayland::1}" == "t" ]] && depends+=('xorg-xwayland')
provides=("$pkgname")
conflicts=("$pkgname-git")
options=('!strip')
_pkgsrc="$pkgname"
source=('git+https://codeberg.org/river/river.git?signed'
#  'git+https://github.com/riverwm/river.git'
  'git+https://github.com/ifreund/zig-pixman.git'
  'git+https://github.com/ifreund/zig-wayland.git'
  'git+https://github.com/swaywm/zig-wlroots.git'
  'git+https://github.com/ifreund/zig-xkbcommon.git'
)

prepare() {
  cd "$_pkgsrc"
  git submodule init
  for dep in pixman wayland wlroots xkbcommon; do
    git config "submodule.deps/zig-$dep.url" "$srcdir/zig-$dep"
  done
  git -c protocol.file.allow=always submodule update
}

pkgver() {
  cd "$_pkgsrc"
  local _tag=$(git tag | sort -rV | head -1)
  local _version"=${_tag#v}"
  local _revision=$(git rev-list --count --cherry-pick "$_tag"...HEAD)
  local _hash=$(git rev-parse --short=7 HEAD)

  printf '%s.r%s.g%s' "${_version:?}" "${_revision:?}" "${_hash:?}"
}

package() {
  depends=('libevdev'  'libxkbcommon'  'mesa'  'pixman'  'wayland'  'wayland-protocols'  'wlroots')
  cd "$_pkgsrc"
  local _zig_options=(
    --prefix '/usr'
    -Doptimize=ReleaseSafe
  )

  [[ "${_build_xwayland::1}" == "t" ]] && _zig_options+=(-Dxwayland)

  DESTDIR="$pkgdir" zig build "${_zig_options[@]}"

  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -Dm644 README.md -t "$pkgdir/usr/share/doc/$pkgname/"
  install -Dm644 contrib/river.desktop -t "$pkgdir/usr/share/wayland-sessions/"

  install -d "$pkgdir/usr/share/$pkgname"
  cp --reflink=auto -fr example "$pkgdir/usr/share/$pkgname/"
}

#---- arch license gpg-key & sha256sums ----

arch=(x86_64)

license=('GPL-3.0-only')

validpgpkeys=('5FBDF84DD2278DB2B8AD8A5286DED400DDFD7A11') # Isaac Freund <mail@isaacfreund.com>

sha256sums=(SKIP SKIP SKIP SKIP SKIP)

##  d80dff839c920990d7fbe52c9baa839d9ff48a18881b2843ab8e33c784341bb2  river-0.3.7.r32.g5436978-03-x86_64.pkg.tar.lz
